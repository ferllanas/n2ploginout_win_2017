//======================================================================================== 
// 
// 
// ADOBE CONFIDENTIAL 
// 
// Copyright 2002 Adobe Systems Incorporated. All Rights Reserved. 
// 
// NOTICE: All information contained herein is, and remains 
// the property of Adobe Systems Incorporated and its suppliers, 
// if any. The intellectual and technical concepts contained 
// herein are proprietary to Adobe Systems Incorporated and its 
// suppliers and may be covered by U.S. and Foreign Patents, 
// patents in process, and are protected by trade secret or copyright law. 
// Dissemination of this information or reproduction of this material 
// is strictly forbidden unless prior written permission is obtained 
// from Adobe Systems Incorporated. 
// 
// 
// MACINTOSH event handler for password widget 
// 
//======================================================================================== 

//#include "VCPlugInHeaders.h" 
#include "VCPlugInHeaders.h"
#include "CIDEditBoxEventHandler.h"
#include "IEvent.h"
#include "VirtualKey.h"
#include "Keyboarddefs.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
//interfaces 

//includes 
#include "TextChar.h" 
#include "N2PInOutID.h" 

class N2PSQLNonBrokenPasswordEventHandler : public CIDEditBoxEventHandler
{
    typedef CIDEditBoxEventHandler inherited;

	public:
		N2PSQLNonBrokenPasswordEventHandler(IPMUnknown *boss); 
		~N2PSQLNonBrokenPasswordEventHandler(); 

		virtual bool16 KeyDown(IEvent* e); 
}; 

CREATE_PMINTERFACE(N2PSQLNonBrokenPasswordEventHandler, kN2PNonBrokenPasswordEventHandlerImpl) 

N2PSQLNonBrokenPasswordEventHandler::N2PSQLNonBrokenPasswordEventHandler(IPMUnknown *boss) : 
inherited(boss)
{ 

} 

N2PSQLNonBrokenPasswordEventHandler::~N2PSQLNonBrokenPasswordEventHandler() 
{ 

} 

bool16 N2PSQLNonBrokenPasswordEventHandler::KeyDown(IEvent* pEvent)
{
    if(pEvent == nil) return kFalse;
    
    if(pEvent->CmdKeyDown())
        return inherited::KeyDown(pEvent);
    
    const VirtualKey vKey = pEvent->GetVirtualKey();
    if( vKey == kVirtualReturnKey || vKey == kVirtualEnterKey)
    {
       /* do{
            InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
            if(myParent == nil) break;
            
            InterfacePtr<IN2PCalTableCellData>	pTableCellData((IN2PCalTableCellData*)myParent->QueryParentFor(IID_IN2PCALTABLECELLDATA));
            if(pTableCellData == nil) break;
            
            InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA));
            if(panel == nil) break;
            
            InterfacePtr<IControlView>		editBoxView(panel->FindWidget(kN2PCalTableCellEditBoxWidgetID), UseDefaultIID());
            if(panel == nil) break;
            
            InterfacePtr<ITextControlData>	selectedChar(editBoxView, UseDefaultIID());
            if(selectedChar == nil) break;
            
            PMString  s = selectedChar->GetString();
            s.SetTranslatable(kFalse);
            
            pTableCellData->SetCellString(WideString(s)); // Set string to selected cell.
            
            InterfacePtr<IControlView> tableCell(panel->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
            if(tableCell == nil) break;
            
            tableCell->Invalidate();
            
        }while(false);*/
        return inherited::KeyDown(pEvent);
    }
    
    
    if( vKey == kVirtualDeleteKey || vKey == kVirtualTabKey || vKey == kVirtualBackspaceKey || vKey == kVirtualEscapeKey
       || vKey == kVirtualRightArrowKey || vKey == kVirtualLeftArrowKey
       || vKey.GetChar() == '0' || vKey.GetChar() == '1' || vKey.GetChar() == '2' || vKey.GetChar() == '3'
       || vKey.GetChar() == '4' || vKey.GetChar() == '5' || vKey.GetChar() == '6' || vKey.GetChar() == '7'
       || vKey.GetChar() == '8' || vKey.GetChar() == '9')
        return inherited::KeyDown(pEvent);
    
    return kTrue; // prevent processing of non numeric keys.
    
}

//And then we go and use it like this (in your dialog .fr): 

/*type kN2PNonBrokenPasswordEditBoxWidgetBoss(kViewRsrcType) 
: TextEditBoxWidget(ClassID = kN2PNonBrokenPasswordEditBoxWidgetBoss) {}; 

NonBrokenPasswordEditBoxWidget 
( 
kLoginPasswordEditBoxWidgetId, kSysEditBoxPMRsrcId, // WidgetId, RsrcId 
kBindNone, 
Frame(105, 100, 280, 120), // Frame 
kTrue, kTrue, // Visible, Enabled 
0, // widget id of nudge button 
0, 0, // small/large nudge amount 
32, // max num chars( 0 = no limit) 
kFalse, // is read only 
kFalse, // notify on every keystroke 
kFalse, // range checking enabled 
kTrue, // blank entry allowed 
0, 0, // upper/lower bounds 
"" // initial text 
),*/