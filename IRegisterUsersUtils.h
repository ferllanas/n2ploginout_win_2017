//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customprefs/IRegisterUsersUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ICstPrfUtils_h__
#define __ICstPrfUtils_h__

// Interface Includes
#include "IPMUnknown.h"
#include "N2PRegisterUsers.h"

/** IRegisterUsersUtils
	These utilities are provided as a convenient way to access
	and manipulate the preferences.

	@ingroup customprefs
	@author John Hake
*/
class IRegisterUsersUtils : public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_IREGISTERUSERSUTILS };

		/**
			Acquires the preferences interface for workspace
			or document workspace
		*/
		virtual IN2PRegisterUsers* QueryCustomPrefs(IPMUnknown*) = 0;
		
		/**
			Creates and intializes a command to modify the preferences
		*/
		virtual ICommand* CreateModPrefsCmd(IN2PRegisterUsers*, bool16, int32,PMString ) = 0;

};

#endif

// End, IRegisterUsersUtils.h

