/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "IN2PSQLUtils.h"
#include "N2PInOutID.h"
#include "CAlert.h"

#include "..\N2PWSDLClient\N2PwsdlcltID.h"
#include "..\N2PWSDLClient\N2PWSDLCltUtils.h"

#include <stdio.h>
#include <stdlib.h>
//#include <sql.h>
//#include <sqlext.h>

#define MAX_BUF_LEN  30000//30000
#define DATA_BUFFER_LEN  8192

/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	author Juan Fernando Llanas Rdz
*/
class N2PSQLUtils : public CPMUnknown<IN2PSQLUtils>
{
public:
	/** Constructor.
		param boss boss object on which this interface is aggregated.
	 */
	N2PSQLUtils (IPMUnknown *boss);
	
	virtual ~N2PSQLUtils() {}



	/**
		crea un nuevo registro o actualiza
	*/
	virtual const bool16 SQLQueryDataBase_WebServices(const PMString& StringConnection,const PMString& SentenciaSQL, K2Vector<PMString>& QueryVector);
	virtual const bool16 SQLSetInDataBase_WebServices(const PMString& StringConnection,const PMString& SentenciaSQL);
	virtual const PMString ReturnItemContentbyNameColumn(const PMString& NameCol, PMString& RegisterContent);
	
	const bool16 TestConnection(const PMString& StringConnection);

	/**
		Query en Base de datos
	
	virtual const bool16 SQLSetInDataBase(const PMString& StringConnection,const PMString& SentenciaSQL);

	virtual const bool16 SQLQueryDataBase(const PMString& StringConnection,const PMString& SentenciaSQL, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 TestConnection(const PMString& StringConnection);
	
	virtual const bool16 ExecProcedure(const PMString& StringConnection,const PMString& SentenciaSQL);
	
	virtual const bool16 StoreProcedureCheckInPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureSaveRevPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureCheckOutPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureLOGINUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureLOGOUTUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureCHECKOUTNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector);

	virtual const bool16 StoreProcedureCHECKINNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector,  const bool16& NotaEnCheckOut);
	
	virtual const bool16 StoreProcedureSET_NOTA_COLO_CADA(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	
	virtual const bool16 StoreProcedureIMPORTARNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureCHECKINELEMENTONOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector, const bool16& NotaEnCheckOut);
 
 	//virtual const bool16 HAYPAPA();
private:
	
	bool16 ejecutaStoreProcedureIMPORTARNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );
	
	bool16 ejecutaStoreProcedureSET_NOTA_COLO_CADA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );

	bool16 ejecutaStoreProcedureCHECKOUTNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );


	bool16 ejecutaStoreProcedureLOGOUTUSER(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );
	 
	bool16 ejecutaStoreProcedureLOGINUSER(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );

	bool16 ejecutaStoreProcedureCheckOutPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );


	bool16 ejecutaStoreProcedureSaveRevPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );

	bool16 ejecutaStoreProcedureCheckInPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );
	
	bool16 SQLInicializaConexion(const PMString& StringConnection,SQLHANDLE& stmt,SQLHANDLE& con,SQLHANDLE& env);
	
	bool16 SQLEjecutaSimpleSentencia(SQLHANDLE& stmt,PMString Sentencia);
	
	bool16 ejecutaStoreProcedureCHECKINNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector , const bool16& NotaEnCheckOut);
	
	bool16 SQLEjecutaQuerySentencia(SQLHANDLE& stmt,PMString Sentencia,K2Vector<PMString>& QueryVector  );
	
	PMString printBytes(char* fieldName, char* field, int fieldLen);
	
	bool16 ejecutaStoreProcedure(SQLHANDLE& stmt,PMString Sentencia);
	
	
	bool16 ejecutaStoreProcedureCHECKINELEMENTONOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector , const bool16& NotaEnCheckOut);
*/
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLUtils, kN2PSQLUtilsImpl)


/* HelloWorld Constructor
*/
N2PSQLUtils::N2PSQLUtils(IPMUnknown* boss) 
: CPMUnknown<IN2PSQLUtils>(boss)
{
	// do nothing.

	/* NOTE: 
		There used to be code that would set the preference value by default.
		That was removed in favor of the workspace responders.
		
		The responders get called, and they will try to setup the value
		for this custom preference.  It is one of the responders that will
		set the correct initial value for the cached preference, fPrefState.
		
		If the global workspace is already created from a previous 
		application session, the ReadWrite() method will set the 
		cached preference, fPrefState. The same ReadWrite() will be called when 
		an existing document is opened.
		
		Refer to CstPrfNewWSResponder.cpp and N2PSQLUtils.cpp
		for details on how to set the workspace defaults. 
	*/
}
 

const bool16 N2PSQLUtils::TestConnection(const PMString& StringConnection)
{
	bool16 retval=kFalse;

	do
	{
		InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																					  (
																					   kN2PwsdlcltUtilsBoss,	// Object boss/class
																					   IN2PWSDLCltUtils::kDefaultIID
																					   )));
		
		if(!	N2PWSDLCltUtil)
		{
			break;
		}
		PMString ResultString="";
		
		//CAlert::InformationAlert("StringConnection"+StringConnection);
		K2Vector<PMString> QueryVector=N2PWSDLCltUtil->MySQLConsulta_function("SELECT * FROM usuario WHERE estatus<90 LIMIT 1",
														   StringConnection);
		
		
		if(QueryVector.Length()>0)
			retval=kTrue;
	}while (false);
	return retval;
}


const bool16 N2PSQLUtils::SQLSetInDataBase_WebServices(const PMString& StringConnection,const PMString& SentenciaSQL)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																					  (
																					   kN2PwsdlcltUtilsBoss,	// Object boss/class
																					   IN2PWSDLCltUtils::kDefaultIID
																					   )));
		
		if(!	N2PWSDLCltUtil)
		{
			break;
		}
		PMString ResultString="";
		N2PWSDLCltUtil->MySQLConsulta_function(SentenciaSQL, StringConnection);
		
			retval=kTrue;
	}while(false);
	
	return retval;
}


const bool16 N2PSQLUtils::SQLQueryDataBase_WebServices(const PMString& StringConnection,const PMString& SentenciaSQL, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																				  (
																				   kN2PwsdlcltUtilsBoss,	// Object boss/class
																				   IN2PWSDLCltUtils::kDefaultIID
																				   )));
	
		if(!	N2PWSDLCltUtil)
		{
			break;
		}
		PMString ResultString="";
		QueryVector=N2PWSDLCltUtil->MySQLConsulta_function(SentenciaSQL,StringConnection);
		
		if(QueryVector.Length()>0)
			retval=kTrue;
	}while(false);
	
	return retval;
}


const PMString N2PSQLUtils::ReturnItemContentbyNameColumn(const PMString& NameCampo, PMString& RegisterContent)
{
	
	
	
	if(RegisterContent.NumUTF16TextChars()<=0)
		return "";
		
	//CAlert::InformationAlert(RegisterContent);
	//CAlert::InformationAlert(NameCampo);
	PMString BuscarString=PMString(NameCampo.GetUTF8String().c_str(), PMString::kDontTranslateDuringCall);
	BuscarString.SetTranslatable(kFalse);
	BuscarString.Append(":");
	PMString Retval=PMString("", PMString::kDontTranslateDuringCall);//"";
	
	bool16 isigualTextoBuscado=kFalse;
	int32 index=0;
	int32 indexNameCampo=0;
	do
	{
		
		
		
		indexNameCampo=RegisterContent.IndexOfString(BuscarString,index);
		if(indexNameCampo>=0)
		{
			//CAlert::InformationAlert("Si se encontro");
			Retval=(RegisterContent.Substring(indexNameCampo,(BuscarString.NumUTF16TextChars())))->GetUTF8String().c_str();
			//CAlert::InformationAlert("Obtiene el texto:"+Retval);
			if(Retval==BuscarString)
			{
				//CAlert::InformationAlert("OK se encontrooooo");
				isigualTextoBuscado=kTrue;
			}
			else
			{
				index=indexNameCampo+BuscarString.NumUTF16TextChars();
			}
		}
	}while(isigualTextoBuscado==kTrue && indexNameCampo==-1);
	
	Retval="";
	if(indexNameCampo==-1)
	{
		Retval=PMString("", PMString::kDontTranslateDuringCall);
	}
	else
	{
		indexNameCampo=indexNameCampo + (BuscarString.NumUTF16TextChars());
		
		int32 indexChar=RegisterContent.IndexOfString("©",indexNameCampo);
		/*if(debug)
		 {
		 PMString CC="";
		 CC.SetTranslatable(kFalse);
		 CC=NameCampo+", ";
		 
		 CC.AppendNumber(indexChar);
		 CC.Append(", ");
		 CC.AppendNumber(indexNameCampo);
		 CC.Append(":  ");
		 CC.Append(RegisterContent);
		 //CAlert::InformationAlert(CC);
		 
		 if((indexChar-indexNameCampo)==1)
		 {
		 CC="";
		 CC.AppendNumber(RegisterContent.GetWChar(indexChar+1).GetValue());
		 CAlert::InformationAlert(CC);
		 }
		 }*/
		if((indexChar-indexNameCampo)>0)
		{
			
			if((indexChar-indexNameCampo)==1)
			{
				if(RegisterContent.GetWChar(indexChar+1).GetValue()>0)
				{
					K2::scoped_ptr<PMString> ptrRightStr(RegisterContent.Substring(indexNameCampo,(indexChar-indexNameCampo-1)));
					if(ptrRightStr)
					{
						Retval=*ptrRightStr;
					}
				}
			}
			else 
			{
				if((indexChar-indexNameCampo)>1)
				{
					K2::scoped_ptr<PMString> ptrRightStr(RegisterContent.Substring(indexNameCampo,(indexChar-indexNameCampo-1)));
					if(ptrRightStr)
					{
						Retval=*ptrRightStr;
					}
				}
				else
					Retval=PMString("", PMString::kDontTranslateDuringCall);
			}			
		}
		else
		{
			Retval=PMString("", PMString::kDontTranslateDuringCall);
		}
	}
	return(Retval);
}