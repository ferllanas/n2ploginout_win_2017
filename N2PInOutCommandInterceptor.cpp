

#include "VCPlugInHeaders.h"

#include "ICommand.h"
#include "ICommandInterceptor.h"

#include "IGraphicFrameData.h"
#include "IHierarchy.h"
#include "ILayoutUIUtils.h"
#include "ILayoutUIUtils.h"

#include "DocumentID.h"
#include "PageItemScrapID.h"
#include "TextID.h"

#include "CAlert.h"

#include "InCopySharedID.h"

#include "N2PInOutID.h"
#include "UpdateStorysAndDBUtilis.h"
#include "N2PsqlLogInLogOutUtilities.h"


/** Implements ICommandInterceptor; purpose is to install the idle task.

	
	@ingroup paneltreeview
*/
class N2PInOutCommandInterceptor : 
	public CPMUnknown<ICommandInterceptor>
{
public:
	/**	Constructor
		@param boss interface ptr from boss object on which this interface is aggregated.
	 */
    N2PInOutCommandInterceptor(IPMUnknown* boss );

	/**	Destructor
	 */
	virtual ~N2PInOutCommandInterceptor() {}

	
	
	virtual InterceptResult		InterceptProcessCommand (ICommand *cmd);
	
	virtual InterceptResult 	InterceptScheduleCommand (ICommand *cmd);
	
	virtual InterceptResult 	InterceptExecuteDynamic (ICommand *cmd);
	
	virtual InterceptResult 	InterceptExecuteImmediate (ICommand *cmd);
	
	
	virtual void 	InstallSelf ();
	
	virtual void 	DeinstallSelf ();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PInOutCommandInterceptor, kN2PInOutCommandInterceptorImpl)

/* Constructor
*/
N2PInOutCommandInterceptor::N2PInOutCommandInterceptor(IPMUnknown* boss) :
    CPMUnknown<ICommandInterceptor>(boss)
{
}


void 	N2PInOutCommandInterceptor::InstallSelf ()
{
	//CAlert::InformationAlert("Commando interceptor Instalado");
}	

void 	N2PInOutCommandInterceptor::DeinstallSelf ()
{
	//CAlert::InformationAlert("Commando interceptor Instalado");
}
	

ICommandInterceptor::InterceptResult 	N2PInOutCommandInterceptor::InterceptExecuteImmediate (ICommand *cmd)
{

	//CAlert::InformationAlert("InterceptExecuteImmediate");
	InterceptResult myResult=kCmdNotHandled;
	
	return myResult;
}


ICommandInterceptor::InterceptResult		N2PInOutCommandInterceptor::InterceptProcessCommand (ICommand *cmd)
{
	//CAlert::InformationAlert("InterceptProcessCommand");
	InterceptResult myResult=kCmdNotHandled;
	 do 
	{
	
		if(!N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			break;
		}
		
		PMString NameCommand="";
		cmd->GetName (&NameCommand);
		if(NameCommand=="Clear" || NameCommand=="Borrado")
		{
			//switch (cmd->GetCreatorID())
			//{
					
			/*Para cuando borra algun elemento un page item, 
				aqui se debe de averiguar si se esta borrando una caja con texto que no este ligada.... 
				Verificar que Story se esta borrando si definitivamente se esta borrando una caja que contenga un story
				completo el cual pertenece a N2P debe de evitarse el borrado
			*/
			//kDeleteFrameCmdBoss
			//kDeletedTextDeleteCmdBoss Cuando se borra texto
			//case kDeletePageItemCmdBoss://kDeleteFrameCmdBoss://kDeleteMultiColumnItemCmdBoss://
			//{
				//CAlert::InformationAlert("Padsa por aqui primero 2 InterceptProcessCommand Delte Page Item");
				//Si este MEdoto devuelve Verdadero significa que se esta intentando borrar una caja de New2Page que contine texto
				
				//CAlert::InformationAlert("23");
				const UIDList& newDocCmdUIDList =cmd->GetItemListReference();
				
				
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
				//CAlert::InformationAlert("25");
				if(UpdateStorys==nil)
				{
					//CAlert::InformationAlert("UpdateStorys");
					break;
				}
				// CAlert::InformationAlert("26");
				if(UpdateStorys->Verifica_BorradoDCaja_D_News2Page(newDocCmdUIDList))		
				{
					myResult=kCmdHandled;//Determina que no debe se ejecutarse el borrado de este page item
					
					//CAlert::InformationAlert(NameCommand);
				}
				//CAlert::InformationAlert("27");	
				break;
			//}
			
				
				
			/*Este se activa cuando intentas borrar Cualquier textFrame a un y cuando estan ligados
			case kDeleteStoryCmdBoss:
			{
				CAlert::InformationAlert("kDeleteStoryCmdBoss");
				myResult=kCmdHandled;
				break;
			}
			
			case kDeleteTextCmdBoss:
			{
				CAlert::InformationAlert("kDeleteTextCmdBoss");
				myResult=kCmdHandled;
				break;
			}
			*/
			//default:
			//{
			//	break;
			//}
			//}
		}
	} while(kFalse);
	
	return myResult;
}
	
ICommandInterceptor::InterceptResult 	N2PInOutCommandInterceptor::InterceptScheduleCommand (ICommand *cmd)
{
	//CAlert::InformationAlert("InterceptScheduleCommand");
	InterceptResult myResult=kCmdNotHandled;
	 do 
	{
	} while(kFalse);
	
	return myResult;
}
ICommandInterceptor::InterceptResult 	N2PInOutCommandInterceptor::InterceptExecuteDynamic (ICommand *cmd)
{
	//CAlert::InformationAlert("InterceptExecuteDynamic");
	InterceptResult myResult=kCmdNotHandled;
	 do 
	{
		
	} while(kFalse);
	
	return myResult;
}	
